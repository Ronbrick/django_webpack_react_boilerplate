This is a simple dev boilerplate using django, react and webpack.

How to use:
git clone
python -m venv env
source env/bin/activate
pip install -r requirements.txt
npm install
./manage.py runserver
npm start


Webpack config:
https://webpack.js.org/concepts/entry-points/
