import React from 'react';
import ReactDOM from 'react-dom';
import '../../css/main-style.css'

class MainMenu extends React.Component {
    render() {
        return (
            <div style={this.styles.navBar}>
                <MenuItem name={'Home'} href={'/'}/>
                <MenuItem name={'About'} href={'/about'}/>
                <MenuItem name={'Projects'} href={'/projects'}/>
            </div>
        );
    }

    styles = {
        navBar: {
            backgroundColor: 'lightgrey',
            color: 'grey',
            padding: '20px',
            display: 'flex'
        }
    }
}

class MenuItem extends React.Component {
    render() {
        return (
            <div>
                <a className='menu-button' href={this.props.href}>
                    {this.props.name}
                </a>
            </div>
        );
    }
}

ReactDOM.render(<MainMenu />, document.getElementById('main-menu'));
